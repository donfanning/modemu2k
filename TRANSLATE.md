# Translating

We use Transifex to translate the output messages. To start translating
modemu2k, ​create an account in Transifex and ask to join a translation
team (or create a new one) at
https://www.transifex.com/na-309/modemu2k/
